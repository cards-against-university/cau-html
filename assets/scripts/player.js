function ajax(method, url, query, body, headers, json) {
	if(!headers) headers={};
	if(!query) query={};
	if(!body) body=null;
	
	let paramStr=Object.keys(query)
		.map(a => encodeURIComponent(a)+'='+encodeURIComponent(query[a]))
		.join('&');
	if(paramStr) url+='?'+paramStr;
	
	return new Promise((ok, ko) => {
		let xhr=new XMLHttpRequest();
		xhr.open(method, url, true);
		if(typeof body=='object') {
			xhr.setRequestHeader('Content-Type', 'application/json');
			body=JSON.stringify(body);
		}
		for(let h in headers) {
			xhr.setRequestHeader(k, headers[k]);
		}
		xhr.addEventListener('load', () => {
			if(xhr.status!=200) {
				return ko(new Error(xhr.status));
			}
			if(json) {
				return ok(JSON.parse(xhr.responseText));
			} else {
				return ok(xhr.responseText);
			}
		});
		xhr.addEventListener('error', () => {
			ko();
		});
		xhr.send(body);
	});
}

function sleep(n) {
	return new Promise((ok, ko) => {
		setTimeout(ok, n);
	});
}

function removeNode(n) {
	n.parentElement.removeChild(n);
}
async function start() {
	let game={};
	game.username=prompt("Select your username");
	try {
		//document.documentElement.requestFullscreen();
	} catch(e) {
		console.error(e);
	}
	removeNode(document.querySelector('#fullscreenprompt'));
	try {
		await ajax('POST', '/api/join', null, {username: game.username});
	} catch(e) {
		alert("Unable to login");
		return location.reload();
	}
	console.log("Logged in!");
	await dealCards();
	document.querySelectorAll('.white').forEach(e => {
		e.addEventListener('click', async () => {
			if(!(game.canplay&&game.state=='playing')) return alert("Can't play a card now");
			let txt=e.innerText.trim();
			if(!confirm(`Really play this card? [${txt.trim()}]`)) return;
			e.classList.remove('text');
			e.innerText='[loading]';
			try {
				await ajax('POST', '/api/play', null, {username: game.username, card: txt});
				game.canplay=false;
			} catch(err) {
				alert("Unable to play card");
				e.innerText=txt;
				e.classList.add('text');
				return false;
			}
			await dealCards();
		});
	});
	console.log("Cards dealt");
	while(true) {
		game.state=await ajax('GET', '/api/state');
		if(game.state=='idle') game.canplay=true;
		document.querySelector('#state').innerText=game.state;
		await sleep(1000);
	}
}
async function dealCards() {
	for(let e of document.querySelectorAll('.white')) {
		if(!e.classList.contains('text')) {
			e.innerText=await ajax('GET', '/api/whitecard');
			e.classList.add('text');
		}
	}
}
