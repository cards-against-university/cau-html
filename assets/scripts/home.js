let state;

function ajax(method, url, query, body, headers, json) {
	if(!headers) headers={};
	if(!query) query={};
	if(!body) body=null;
	
	let paramStr=Object.keys(query)
		.map(a => encodeURIComponent(a)+'='+encodeURIComponent(query[a]))
		.join('&');
	if(paramStr) url+='?'+paramStr;
	
	return new Promise((ok, ko) => {
		let xhr=new XMLHttpRequest();
		xhr.open(method, url, true);
		if(typeof body=='object') {
			xhr.setRequestHeader('Content-Type', 'application/json');
			body=JSON.stringify(body);
		}
		for(let h in headers) {
			xhr.setRequestHeader(k, headers[k]);
		}
		xhr.addEventListener('load', () => {
			if(xhr.status!=200) {
				return ko(new Error(xhr.status));
			}
			if(json) {
				return ok(JSON.parse(xhr.responseText));
			} else {
				return ok(xhr.responseText);
			}
		});
		xhr.addEventListener('error', () => {
			ko();
		});
		xhr.send(body);
	});
}

function removeNode(n) {
	n.parentElement.removeChild(n);
}

function sleep(n) {
	return new Promise((ok, ko) => {
		setTimeout(ok, n);
	});
}

let game={};
window.addEventListener('load', async () => {
	const container=document.querySelector('.conteneur');
	const step1=document.querySelector('#step1');
	const step2=document.querySelector('#step2');
	const step3=document.querySelector('#step3');
	game.state='idle';
	game.blackcard=null;
	game.cards=[];
	
	async function updateState() {
		game.state=await ajax('GET', '/api/state');
	}
	async function getInput() {
		game.input=await ajax('GET', '/api/input');
		if(game.input=='none') game.input=null;
	}
	
	async function updateCards() {
		game.cards=await ajax('GET', '/api/cards', null, null, null, true);
		while(container.firstChild) removeNode(container.firstChild);
		for(let card of game.cards) {
			let div=document.createElement('div');
			div.classList.add('white');
			div.innerText=card;
			container.appendChild(div);
			div.scrollIntoView();
		}
	}
	
	async function drawBlackcard() {
		let blackcard=await ajax('GET', '/api/blackcard');
		blackcard=blackcard.replace(/<white>/, '_______');
		game.blackcard=blackcard.replace(/\s*\|\|\s*/g, '\n');
		document.querySelector('#black').innerText=game.blackcard;
		document.querySelector('#black2').innerText=game.blackcard;
		document.querySelector('#black3').innerText=game.blackcard;
	}
	
	async function drawWhitecard() {
		let whitecard=await ajax('GET', '/api/selectedCard');
		document.querySelector('#white3').innerText=whitecard;
	}
	
	(async () => {
		while(true) {
			await updateState();
			if(!game.blackcard) await drawBlackcard();
			if(game.state=='idle') {
				step1.classList='visible';
				step2.classList='hidden';
				step3.classList='hidden';
				await getInput();
				if(game.input=='middle') {
					await ajax('GET', '/api/start');
					continue;
				}
			}
			if(game.state=='playing') {
				await updateCards();
				step1.classList='hidden';
				step2.classList='visible';
				step3.classList='hidden';
			}
			if(game.state=='selecting') {
				if(game.cards.length==0) {
					delete game.blackcard;
					await ajax('GET', '/api/reset');
					continue;
				}
				if(!container.querySelector('.target')) {
					container.querySelector('.white').classList.add('target');
				}
				await getInput()
				if(game.input=='middle') {
					try {
						await ajax('POST', '/api/select', null, {card: container.querySelector('.target').innerText});
					} catch(e) {
						console.error(e);
						await ajax('GET', '/api/reset');
					}
				} else if(game.input=='left') {
					let target=container.querySelector('.target');
					let prev=target.previousElementSibling;
					if(!prev) prev=container.lastElementChild;
					target.classList.remove('target');
					prev.classList.add('target');
				} else if(game.input=='right') {
					let target=container.querySelector('.target');
					let next=target.nextElementSibling;
					if(!next) next=container.firstElementChild;
					target.classList.remove('target');
					next.classList.add('target');
				}
			}
			if(game.state=='ended') {
				await drawWhitecard();
				step1.classList='hidden';
				step2.classList='hidden';
				step3.classList='visible';
				await getInput();
				if(game.input=='middle') {
					await ajax('GET', '/api/reset');
					game.blackcard=null;
					continue;
				}
			}
			await sleep(1000);
		}
	})();
});
