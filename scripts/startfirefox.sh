#!/bin/sh
awesome&
xhost +
while ! [ `curl 127.0.0.1/api/ping` = "pong" ]; do
	sleep 1s
done
sudo -u pi caffeinate `dirname $0`/browser.sh
